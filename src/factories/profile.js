"use strict";

let ProfileFactory = function() {};

/**
 * Create a profile from the data returned by the crawler
 * @param object
 */
ProfileFactory.createFromObject = function(object) {
    if (typeof object !== 'object') throw new Error('object must be of type "object"');
    if (!object.hasOwnProperty('profile_type')) throw new Error('object must have a "profile_type" property');
    if (!object.hasOwnProperty('data')) throw new Error('object must have a "data" property');
    if (typeof object.data !== 'object') throw new Error('object.data must be of type "object"');

    let ProfileClass = null;
    let ProfileUtils = require('utils/profile');

    switch (object.profile_type) {
        case ProfileUtils.profiles.GITHUB:
                ProfileClass = require('entities/profile/api/github');
            break;
        default:
            throw new Error('profile_type mismatch');
            break;
    }

    let profile = new ProfileClass();
    let nbProperties = object.data.length;
    let StringUtils = require('utils/string');

    // Set properties from object
    for (let key in object.data) {
        let formattedKey = StringUtils.toCamelCase(key);

        if (profile.apiData.hasOwnProperty(formattedKey)) {
            profile.apiData[formattedKey] = object.data[key];
        }
    }

    return profile;
};

module.exports = ProfileFactory;