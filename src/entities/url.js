"use strict";

/**
 * Represents an url
 * @param url
 * @constructor
 */
let Url = function(url) {
    this.url = typeof url !== 'undefined' ? url : null;
};

module.exports = Url;