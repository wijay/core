"use strict";

let md5 = require('blueimp-md5');

/**
 * Represents an email and its associated values
 */
let Email = function(email) {
    this.email = null;
    this.md5 = null;

    Object.defineProperties(this, {
        email: {
            get: function () {
                return this._email;
            },
            set: function (val) {
                this._email = val;
                this._md5 = md5(val); // Set the md5 when the email is updated
            }
        },
        md5: {
            get: function() {
                return this._md5;
            },
            set: function(val) {
                if (this._md5 !== val) this._email = null; // Reset the email when a new md5 is set
                this._md5 = val;
            }
        }
    });

    if (typeof email !== 'undefined') this.email = email;
};

module.exports = Email;