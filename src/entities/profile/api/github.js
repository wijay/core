"use strict";

let AbstractProfile = require('entities/profile/api/abstract');
let DateUtils = require('utils/date');

/**
* A Github profile with API data
*/
let GithubProfile = function() {
    AbstractProfile.call(this);

    this.apiData.login = null;
    this.apiData.id = null;
    this.apiData.avatarUrl = null;
    this.apiData.type = null;
    this.apiData.name = null;
    this.apiData.company = null;
    this.apiData.blog = null;
    this.apiData.location = null;
    this.apiData.email = null;
    this.apiData.hireable = null;
    this.apiData.bio = null;
    this.apiData.publicRepos = null;
    this.apiData.publicGists = null;
    this.apiData.followers = null;
    this.apiData.following = null;
    this.apiData.createdAt = null;
    this.apiData.updatedAt = null;

    this.parsableData = {
        email: ['apiData.email', 'apiData.bio'],
        url: ['apiData.blog', 'apiData.bio']
    };

    DateUtils.defineDateTimeProperties(this, this.apiData, ['createdAt', 'updatedAt']);
};

module.exports = GithubProfile;