"use strict";

/**
* Base profile for Api-based profiles
*/
let AbstractApiProfile = function() {
    // Api data, to be separated from other business data
    this.apiData = {};
    this.emails = [];
    this.urls = [];
};

module.exports = AbstractApiProfile;