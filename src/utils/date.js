"use strict";

let DateUtils = function() {};

/*
* Convert a datetime in a string into a timestamp
*/
DateUtils.convertDateStringToTimestamp = function(str) {
    if (typeof str !== 'string') throw new Error('Invalid parameter: must be a string');

    let timestamp = Date.parse(str);

    if (isNaN(timestamp)) throw new Error('Invalid parameter: must be parsable by Date');

    return timestamp / 1000;
};

/*
* Define datetime properties accessors for an object inside a context
*/
DateUtils.defineDateTimeProperties = function(context, object, properties) {
    if (typeof context !== 'object') throw new Error('Invalid parameter: "context" must be an object');
    if (typeof object !== 'object') throw new Error('Invalid parameter: "object" must be an object');
    if (!properties instanceof Array) throw new Error('Invalid parameter: "properties" must be an array');

    let nbProperties = properties.length;
    let definedProperties = {};

    for (let i = 0; i < nbProperties; i++) {
        let propertyName = properties[i];

        if (!object.hasOwnProperty(propertyName)) throw new Error('Missing property "'+propertyName+'" from object');

        definedProperties[propertyName] = {
            get: function() {
                return object['_'+propertyName];
            },
            set: function(val) {
                object['_'+propertyName] = DateUtils.convertDateStringToTimestamp(val);
            }
        };
    }

    Object.defineProperties.call(context, object, definedProperties);
};

module.exports = DateUtils;