"use strict";

let ArrayUtils = function() {};

/**
* Remove duplicate values from an array
*/
ArrayUtils.removeDuplicates = function(array) {
    let result = [];
    let nbItems = array.length;

    for (let i = 0; i < nbItems; i++) {
        if (result.indexOf(array[i]) < 0) result.push(array[i]);
    }

    return result;
};

module.exports = ArrayUtils;