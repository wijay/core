"use strict";

let StringUtils = function() {};

/**
* Convert a text-and-underscore-based word into a camelCase word
*/
StringUtils.toCamelCase = function(word) {
     return word.replace(/_(\w)/g, function (x, chr) {
         return chr.toUpperCase();
     })
};

module.exports = StringUtils;