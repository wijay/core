"use strict";

let ObjectUtils = function() {};

/**
* Check for nested object properties inside an object (object.a.b.c)
*/
ObjectUtils.hasNestedProperty = function(object) {
    let args = Array.prototype.slice.call(arguments, 1);
    let nbArgs = args.length;

    for (let i = 0; i < nbArgs; i++) {
        if (!object.hasOwnProperty(args[i])) return false;
        object = object[args[i]];
    }

    return true;
};

/**
* Returns the nest property of an object (object.a.b.c)
*/
ObjectUtils.getNestedProperty = function(object, property) {
    return property.split('.').reduce(function(prev, curr) {
        return prev[curr];
    }, object);
};

module.exports = ObjectUtils;