"use strict";

let ObjectUtils = require('utils/object');
let ArrayUtils = require('utils/array');
let Url = require('entities/url');

/**
 * @constructor
 */
let UrlParser = function() {
    this.findPattern = /(?:(?:(?:http|https)\:\/\/)|(?:www\.))[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,5}(?:\/\S*)?/g;
    //this.sanitizePattern = /(?:http\:\:\/\/+)/i; // To go from "abc+test@def.com" to "abc@def.com"
};

/**
 * Get the urls from an object's "parsableData.url" propety using a regex pattern
 * @param object
 * @returns {Array}
 */
UrlParser.prototype.parseObject = function(object) {
    if (!ObjectUtils.hasNestedProperty(object, 'parsableData', 'url')) throw new Error('Object must define the "parsableData.url" property');
    if (!object.parsableData.url instanceof Array) throw new Error('parsableData.url must be an array');

    let nbFields = object.parsableData.url.length;
    let fullString = '';

    for (let i = 0; i < nbFields; i++) {
        let fieldValue = ObjectUtils.getNestedProperty(object, object.parsableData.url[i]);
        fullString += fieldValue ? fieldValue + ' ' : '';
    }

    let found = [];

    if (fullString) {
        found = fullString.match(this.findPattern);
        found = ArrayUtils.removeDuplicates(found);
    }

    let nbFound = found.length;

    for (let i = 0; i < nbFound; i++) {
        let url = new Url(this.sanitizeUrl(found[i]));
        found[i] = url;
    }

    return found;
};

/**
 * Sanitize an url
 * @param url
 * @returns {XML|void|string}
 */
UrlParser.prototype.sanitizeUrl = function(url) {
    return url.match('^https?:\/\/') ? url : 'http://' + url;
};

module.exports = UrlParser;