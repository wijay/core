"use strict";

let ObjectUtils = require('utils/object');
let ArrayUtils = require('utils/array');
let Email = require('entities/email');

/**
 * @constructor
 */
let EmailParser = function() {
    this.findPattern = /[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/g;
    this.sanitizePattern = /(?:[\+]{1}[_a-z0-9-]+)/i; // To go from "abc+test@def.com" to "abc@def.com"
};

/**
 * Get the email addresses from an object's "parsableData.email" propety using a regex pattern
 * @param object
 * @returns {Array}
 */
EmailParser.prototype.parseObject = function(object) {
    if (!ObjectUtils.hasNestedProperty(object, 'parsableData', 'email')) throw new Error('Object must define the "parsableData.email" property');
    if (!object.parsableData.email instanceof Array) throw new Error('parsableData.email must be an array');

    let nbFields = object.parsableData.email.length;
    let fullString = '';

    for (let i = 0; i < nbFields; i++) {
        let fieldValue = ObjectUtils.getNestedProperty(object, object.parsableData.email[i]);
        fullString += fieldValue ? fieldValue + ' ' : '';
    }

    let found = [];

    if (fullString) {
        found = fullString.match(this.findPattern);
        found = ArrayUtils.removeDuplicates(found);
    }

    let nbFound = found.length;

    for (let i = 0; i < nbFound; i++) {
        let email = new Email(this.sanitizeEmail(found[i]));
        found[i] = email;
    }

    return found;
};

/**
 * Sanitize an email
 * @param email
 * @returns {XML|void|string}
 */
EmailParser.prototype.sanitizeEmail = function(email) {
    return email.replace(this.sanitizePattern, '');
};

module.exports = EmailParser;