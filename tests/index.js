"use strict";

// Pour lancer : node_modules\.bin\mocha tests\index.js

//var QUnit = require(__dirname+'/../node_modules/qunitjs/qunit/qunit');
var assert = require('assert');
describe('Array', function() {
  describe('#indexOf()', function () {
    it('should return -1 when the value is not present', function () {
      assert.equal(-1, [1,2,3].indexOf(5));
      assert.equal(-1, [1,2,3].indexOf(0));
    });
  });
});