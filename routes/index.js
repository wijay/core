"use strict";

let express = require('express');
let router = express.Router();
let Email = require('entities/email');

let ProfileFactory = require('factories/profile');
let EmailParser = require('services/parsers/email');
let UrlParser = require('services/parsers/url');

router.get('/', function(req, res) {
    let fs = require('fs');
    let data = JSON.parse(fs.readFileSync('data_fixtures/aziule.github.json', 'utf-8'));
    let emailParser = new EmailParser();
    let urlParser = new UrlParser();
    let json = [];

    let profile = ProfileFactory.createFromObject(data);
    profile.emails = emailParser.parseObject(profile);
    profile.urls = urlParser.parseObject(profile);
    json.push(profile);


    profile = ProfileFactory.createFromObject(JSON.parse(fs.readFileSync('data_fixtures/aziule.github.json', 'utf-8')));
    profile.emails = emailParser.parseObject(profile);
    json.push(profile);
    console.log(profile);


    /*
    var email = new Email();
    email.email = 'abc.com';
    json.push([email.email, email.md5]);
    email.email = 'azeazeaeaze.com';
    json.push([email.email, email.md5]);
    console.log(email);
    var email2 = new Email();
    email2.email = 'yo.com';
    json.push([email2.email, email2.md5]);
    json.push([email.email, email.md5]);
    */

/*
    data = JSON.parse(fs.readFileSync('data_fixtures/a.github.json', 'utf-8'));
    profile = ProfileFactory.createFromObject(data);
    json.push(profile);
*/
    res.send(json);
});

module.exports = router;
